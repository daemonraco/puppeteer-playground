"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const puppeteer_handler_1 = require("./includes/puppeteer.handler");
const tools_1 = require("./includes/tools");
const DEBUG = process.env.DEBUG !== undefined;
let errors = [];
const pageParser = async (params) => {
    const results = {
        title: null,
        errors: [],
    };
    const obj = document.querySelector('head title');
    results.title = obj ? obj.textContent : null;
    results.errors.push('no errors found');
    return results;
};
(async () => {
    try {
        const handler = new puppeteer_handler_1.PuppeteerHandler();
        const data = await handler.getUrl(`https://twitter.com/home`, pageParser, { errors });
        console.log(`DEBUG data`, JSON.stringify(data, null, 2));
        if (DEBUG) {
            await tools_1.waitEnter();
        }
        await handler.close();
    }
    catch (err) {
        console.error(err);
    }
    process.exit();
})();
