"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const puppeteer = require('puppeteer');
const DEBUG = process.env.DEBUG !== undefined;
class PuppeteerHandler {
    constructor() {
        this._browser = null;
        this._page = null;
    }
    async close() {
        await this._browser.close();
        this._browser = null;
        this._page = null;
    }
    async getUrl(pageUrl, evaluator = null, ...args) {
        if (evaluator === null) {
            evaluator = () => {
                const result = {};
                for (const section of ['head', 'body']) {
                    const obj = document.querySelector(section);
                    result[section] = obj ? obj.innerHTML : null;
                }
                return result;
            };
        }
        await this._loadAssets();
        await this._page.goto(pageUrl, { waitUntil: 'networkidle0' });
        return this._page.evaluate(evaluator, ...args);
    }
    async _loadAssets() {
        if (this._browser === null) {
            this._browser = await puppeteer.launch({
                headless: !DEBUG,
            });
            this._page = await this._browser.newPage();
            await this._page.emulateTimezone('America/New_York');
            await this._page.setExtraHTTPHeaders({
                'Accept-Language': 'en-US,en',
            });
        }
    }
}
exports.PuppeteerHandler = PuppeteerHandler;
