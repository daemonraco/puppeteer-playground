"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const readline = require('readline');
exports.waitEnter = async () => {
    const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout
    });
    await new Promise(r => {
        rl.question('Press enter to continue ', (answer) => {
            rl.close();
            r();
        });
    });
};
