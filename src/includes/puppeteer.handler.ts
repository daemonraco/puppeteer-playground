const puppeteer = require('puppeteer');

const DEBUG = process.env.DEBUG !== undefined;

export class PuppeteerHandler {
    //
    // Properties.
    protected _browser: any = null;
    protected _page: any = null;
    //
    // Public methods.
    public async close(): Promise<void> {
        await this._browser.close();
        this._browser = null;
        this._page = null;
    }
    public async getUrl(pageUrl: string, evaluator: Function | null = null, ...args: any[]): Promise<any> {
        if (evaluator === null) {
            evaluator = (): any => {
                const result: any = {};

                for (const section of ['head', 'body']) {
                    const obj: HTMLElement | null = document.querySelector(section);
                    result[section] = obj ? obj.innerHTML : null;
                }

                return result;
            };
        }

        await this._loadAssets();
        await this._page.goto(pageUrl, { waitUntil: 'networkidle0' });
        return this._page.evaluate(evaluator, ...args);
    }
    //
    // Protected methods.
    protected async _loadAssets(): Promise<void> {
        if (this._browser === null) {
            this._browser = await puppeteer.launch({
                headless: !DEBUG,
            });
            this._page = await this._browser.newPage();

            await this._page.emulateTimezone('America/New_York');
            await this._page.setExtraHTTPHeaders({
                'Accept-Language': 'en-US,en',
            });
        }
    }
}