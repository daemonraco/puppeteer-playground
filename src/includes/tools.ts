const readline = require('readline');

export const waitEnter = async () => {
    const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout
    });
    await new Promise(r => {
        rl.question('Press enter to continue ', (answer: string): void => {
            rl.close();
            r();
        });
    });
};