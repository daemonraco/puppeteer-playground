/**
 * @file index.ts
 */
import { PuppeteerHandler } from './includes/puppeteer.handler';
import { waitEnter } from './includes/tools';

const DEBUG = process.env.DEBUG !== undefined;

let errors: string[] = [];
const pageParser = async (params: any): Promise<any> => {
    const results: any = {
        title: null,
        errors: [],
    };

    const obj: HTMLElement | null = document.querySelector('head title');
    results.title = obj ? obj.textContent : null;

    results.errors.push('no errors found');

    return results;
};

(async () => {
    try {
        const handler = new PuppeteerHandler();

        const data = await handler.getUrl(`https://twitter.com/home`, pageParser, { errors });

        console.log(`DEBUG data`, JSON.stringify(data, null, 2));
        if (DEBUG) {
            await waitEnter();
        }

        await handler.close();
    } catch (err) {
        console.error(err);
    }

    process.exit();
})();
